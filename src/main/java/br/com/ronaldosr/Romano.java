package br.com.ronaldosr;

public class Romano {

    private int valor;

    public Romano() {
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public String converterParaRomano(int numero) {

        if (validarNumeracao(numero)) {
            String[] algarismosRomanos = { "I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M" };
            Integer[] numerosArabicos = {1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000};

            String resultado = "";

            for (int i = algarismosRomanos.length; i >=1; i--) {
                while (numero >= numerosArabicos[i-1]) {
                    numero -= numerosArabicos[i-1];
                    resultado += algarismosRomanos[i-1];
                }
            }
            return resultado;

        } else {
            throw new RuntimeException("Conversão permitida para números entre 1 e 3999");
        }

    }

    public boolean validarNumeracao(int numero) {
        if (numero > 0 && numero <= 3999) {
            return true;
        } else {
            return false;
        }
    }
}
