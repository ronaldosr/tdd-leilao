package br.com.ronaldosr;

import java.util.*;

public class Leiloeiro {

    private String nome;
    private List<Leilao> leiloes = new ArrayList<>();

    public Leiloeiro() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Leilao> getLeiloes() {
        return leiloes;
    }

    public void setLeiloes(List<Leilao> leiloes) {
        this.leiloes = leiloes;
    }

    @Override
    public String toString() {
        return "Leiloeiro{" +
                "nome='" + nome + '\'' +
                ", leiloes=" + leiloes +
                '}';
    }

    public Lance obterMaiorLance() {
        Lance maiorLance = new Lance();

        for (Leilao leilao : leiloes) {
            maiorLance = leilao.getLances().get(0);

            for (Lance lance : leilao.getLances()) {
                if(lance.getValor() > maiorLance.getValor()) {
                    maiorLance = lance;
                }
            }
        }
        return maiorLance;
    }

    public void iniciarLeilao (String nome, List<Leilao> leiloes) {
        this.nome = nome;
        this.leiloes = leiloes;
    }

}
