package br.com.ronaldosr;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Leilao {

    private List<Lance> lances;

    public Leilao() {
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    @Override
    public String toString() {
        return "Leilao{" +
                "lances=" + lances +
                '}';
    }

    public void adicionarNovoLance(Lance lance) {
        this.lances.add(lance);
    }

    public boolean eLanceMenorQueAnterior(Lance lance2) {
        for (Lance lance : this.lances) {
            if (lance2.getValor() < lance.getValor()) {
                return true;
            }
        }
        return false;
    }

    public Lance gerarLancesAleatorios() {
        Random random = new Random();
        double valor = random.nextDouble() * 100;
        double maiorValor = valor;
        Lance maiorLance = new Lance();

        for (int i = 1; i <=3; i++) {
            Usuario usuario = new Usuario();
            usuario.novoUsuario(i, "Usuario_0" + i);
            Lance lance = new Lance();
            if (valor >= maiorValor) {
               maiorLance.criarLance(usuario, valor);
            }
            lance.criarLance(usuario, valor);
            this.adicionarNovoLance(lance);
            valor = random.nextDouble() * 100;
        }
        return maiorLance;
    }

}
