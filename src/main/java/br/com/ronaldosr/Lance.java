package br.com.ronaldosr;

public class Lance {

    private Usuario usuario;
    private double valor;

    public Lance() {
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Lance{" +
                "usuario=" + usuario +
                ", valor=" + valor +
                '}';
    }

    public void criarLance(Usuario usuario, double valor) {
        this.usuario = usuario;
        this.valor = valor;
    }
}
