package br.com.ronaldosr;

public class Usuario {

    private int id;
    private String nome;

    public Usuario() {
    }

    public Usuario(String s, int i) {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                '}';
    }

    public void novoUsuario (int id, String nome) {
        this.setId(id);
        this.setNome(nome);
    }
}
