package br.com.ronaldosr;

import org.junit.jupiter.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import java.util.ArrayList;
import java.util.List;

public class LeilaoTeste {

    Leilao leilao = new Leilao();
    Usuario usuario = new Usuario();

    @Before
    public void setUpTeste() {
        List<Lance> lances = new ArrayList<>();
        this.leilao.setLances(lances);
    }

    @Test
    public void testarAdicaoNovoLance() {
        this.usuario.novoUsuario(1, "Usuário 01");
        Lance lance = new Lance();
        lance.criarLance(usuario,1234.56 );
        leilao.adicionarNovoLance(lance);

        Assertions.assertTrue(leilao.getLances().contains(lance));
    }

    @Test
    public void testarLanceDadoMenorLanceAnterior() {
        this.usuario.novoUsuario(1, "Usuário 01");

        Lance lance = new Lance();
        lance.criarLance(usuario,1234.56 );
        leilao.adicionarNovoLance(lance);

        Lance lance1 = new Lance();
        lance1.criarLance(usuario,1234.51);
        leilao.adicionarNovoLance(lance1);

        Assertions.assertTrue(leilao.eLanceMenorQueAnterior(lance1));
    }

    @Test
    public void testarLanceDadoIgualLanceAnterior() {
        this.usuario.novoUsuario(1, "Usuário 01");

        Lance lance = new Lance();
        lance.criarLance(usuario,1234.56 );
        leilao.adicionarNovoLance(lance);

        Lance lance1 = new Lance();
        lance1.criarLance(usuario,1234.56);
        leilao.adicionarNovoLance(lance1);

        Assertions.assertFalse(leilao.eLanceMenorQueAnterior(lance1));
    }

    @Test
    public void testarLanceDadoMaiorIgualLanceAnterior() {
        this.usuario.novoUsuario(1, "Usuário 01");

        Lance lance = new Lance();
        lance.criarLance(usuario,1234.56 );
        leilao.adicionarNovoLance(lance);

        Lance lance1 = new Lance();
        lance1.criarLance(usuario,2345);
        leilao.adicionarNovoLance(lance1);

        Assertions.assertFalse(leilao.eLanceMenorQueAnterior(lance1));
    }
}
