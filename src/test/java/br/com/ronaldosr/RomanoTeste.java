package br.com.ronaldosr;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class RomanoTeste {
 
    Romano romano = new Romano();

    @Test
    public void testarConversaoNumeroUmParaRomano() {
        int numero = 1;
        String resultado = romano.converterParaRomano(numero);
        Assertions.assertEquals("I", resultado);
    }

    @Test
    public void testarConversaoNumeroQuatroParaRomano() {
        int numero = 4;
        String resultado = romano.converterParaRomano(numero);
        Assertions.assertEquals("IV", resultado);
    }

    @Test
    public void testarConversaoNumeroCincoParaRomano() {
        int numero = 5;
        String resultado = romano.converterParaRomano(numero);
        Assertions.assertEquals("V", resultado);
    }

    @Test
    public void testarConversaoNumeroNoveParaRomano() {
        int numero = 9;
        String resultado = romano.converterParaRomano(numero);
        Assertions.assertEquals("IX", resultado);
    }

    @Test
    public void testarConversaoNumeroDezParaRomano() {
        int numero = 10;
        String resultado = romano.converterParaRomano(numero);
        Assertions.assertEquals("X", resultado);
    }

    @Test
    public void testarConversaoNumeroQuarentaParaRomano() {
        int numero = 40;
        String resultado = romano.converterParaRomano(numero);
        Assertions.assertEquals("XL", resultado);
    }

    @Test
    public void testarConversaoNumeroCinquentaParaRomano() {
        int numero = 50;
        String resultado = romano.converterParaRomano(numero);
        Assertions.assertEquals("L", resultado);
    }

    @Test
    public void testarConversaoNumeroNoventaParaRomano() {
        int numero = 90;
        String resultado = romano.converterParaRomano(numero);
        Assertions.assertEquals("XC", resultado);
    }

    @Test
    public void testarConversaoNumeroCemParaRomano() {
        int numero = 100;
        String resultado = romano.converterParaRomano(numero);
        Assertions.assertEquals("C", resultado);
    }

    @Test
    public void testarConversaoNumeroQuatrocentosParaRomano() {
        int numero = 400;
        String resultado = romano.converterParaRomano(numero);
        Assertions.assertEquals("CD", resultado);
    }

    @Test
    public void testarConversaoNumeroQuinhentosParaRomano() {
        int numero = 500;
        String resultado = romano.converterParaRomano(numero);
        Assertions.assertEquals("D", resultado);
    }

    @Test
    public void testarConversaoNumeroNovecentosParaRomano() {
        int numero = 900;
        String resultado = romano.converterParaRomano(numero);
        Assertions.assertEquals("CM", resultado);
    }

    @Test
    public void testarConversaoNumeroMilParaRomano() {
        int numero = 1000;
        String resultado = romano.converterParaRomano(numero);
        Assertions.assertEquals("M", resultado);
    }

    @Test
    public void testarConversaoNumeroZeroParaRomano() {
        int numero = 0;
        Assertions.assertThrows(RuntimeException.class, () -> {
            romano.converterParaRomano(numero);
        });
    }

    @Test
    public void testarConversaoNumeroQuatroMilParaRomano() {
        int numero = 4000;
        Assertions.assertThrows(RuntimeException.class, () -> {
            romano.converterParaRomano(numero);
        });
    }
}
