package br.com.ronaldosr;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LeiloeiroTeste {

    Leiloeiro leiloeiro = new Leiloeiro();
    Usuario usuario = new Usuario();

    @Test
    public void obterMaiorLance() {
        Leilao leilao = new Leilao();
        List<Lance> lances = new ArrayList<>();
        leilao.setLances(lances);

        Lance maiorLance = leilao.gerarLancesAleatorios();

        List<Leilao> leiloes = new ArrayList<>();

        leiloes.add(leilao);
        leiloeiro.iniciarLeilao("Leiloeiro", leiloes);

        Lance resultado = leiloeiro.obterMaiorLance();
        Assertions.assertTrue(maiorLance.getUsuario().equals(resultado.getUsuario()) &&
                maiorLance.getValor() == resultado.getValor());

    }
}
